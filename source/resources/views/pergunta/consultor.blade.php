@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row margin">
            <div class="col-md-4">
                <div class="info-box bg-red-gradient">
                    <span class="info-box-icon"><i class="fa fa-commenting"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Perguntas Sem Resposta</span>
                        <span class="info-box-number">{{$qtdPerguntasSemResposta}}</span>
                    </div>

                    <div class="progress">
                        <div class="progress-bar"
                             style="width: {{(int)($qtdPerguntasSemResposta/$qtdTotalPerguntas*100)}}%">

                        </div>
                    </div>

                    <span class="progress-description">
                        {{(int) ($qtdPerguntasSemResposta/$qtdTotalPerguntas*100)}}% estão sem resposta
                    </span>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-4">
                <div class="info-box bg-green-gradient">
                    <span class="info-box-icon"><i class="fa  fa-comments"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Respondidas Por Mim</span>
                        <span class="info-box-number">{{$qtdPerguntasRespondidasConsultor}}</span>
                    </div>

                    <div class="progress">
                        <div class="progress-bar"
                             style="width: {{(int)($qtdPerguntasRespondidasConsultor/$qtdPerguntasRespondidas*100)}}%">

                        </div>
                    </div>

                    <span class="progress-description">
                        {{(int)($qtdPerguntasRespondidasConsultor/$qtdPerguntasRespondidas)*100}}% das
                        perguntas respondidas
                    </span>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-4">
                <div class="info-box bg-yellow-gradient">
                    <span class="info-box-icon"><i class="fa  fa-hourglass"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tempo Médio de Resposta</span>
                        <span class="info-box-number">41,410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

        </div>

        <div class="row margin">
            <div class="box-body">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th style="width: 10px"># Cód</th>
                        <th>Pergunta</th>
                        <th>Tempo Restante</th>
                        <th style="width: 40px">Tempo</th>
                    </tr>
                    @foreach($perguntas as $pergunta)
                        <?php
                        date_default_timezone_set('America/Sao_Paulo');
                        $data = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pergunta->dtHoraPergunta);
                        $perguntadoHa = $data->diffInHours(\Carbon\Carbon::now());

                        if($perguntadoHa < 24){
                            $badgeColor = "green";
                        }elseif($perguntadoHa < 48){
                            $badgeColor = "yellow";
                        }else{
                            $badgeColor = "red";
                        }

                        ?>
                        <tr class="{{$perguntadoHa>72?"danger":""}}">
                            <td><a href="{{url("/pergunta/".$pergunta->id."/")}}">{{$pergunta->id}}</a></td>
                            <td>
                                <a href="{{url("/pergunta/".$pergunta->id."/")}}">
                                    {{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}
                                </a>
                            </td>
                            <td class="col-md-2">
                                <div class="progress progress-xs">
                                    <div
                                            class="progress-bar progress-bar-{{$badgeColor}}"
                                            style="width: {{(int)($perguntadoHa/72*100)>100?100:$perguntadoHa/72*100}}%">

                                    </div>
                                </div>
                            </td>

                            <td><span class="badge bg-{{$badgeColor}}">{{ $perguntadoHa }}</td></span>
                        </tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection