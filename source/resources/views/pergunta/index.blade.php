@extends('layouts.app')

@section('content')
 <div class="container-fluid">
 <div class="row margin">
  <div class="col-md-2">
   <a href="{{url("/pergunta/create")}}">
    <button type="button" class="btn btn-block btn-primary btn-lg">NOVA PERGUNTA</button>
   </a>
  </div>
 </div>
 <div class="row margin">
  <div id="perguntasSemResposta">
  <div class="box">
   <div class="box-header">
    <h3 class="box-title">Minhas Perguntas Não Respondidas</h3>

    <div class="box-tools">
     <ul class="pull-right">
      <a href="#" class="">Ver Todas</a>
     </ul>
    </div>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
    <table class="table table-striped">
     <tr>
      <th style="width: 10px"># Código</th>
      <th>Pergunta</th>
      <th></th>
      <th style="width: 40px">Tempo</th>
     </tr>
     @foreach($perguntasSemResposta as $pergunta)
            <?php
            date_default_timezone_set('America/Sao_Paulo');
            $data = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pergunta->dtHoraPergunta);
            $perguntadoHa = $data->diffInHours(\Carbon\Carbon::now());

            if($perguntadoHa < 24){
                $badgeColor = "green";
            }elseif($perguntadoHa < 48){
                $badgeColor = "yellow";
            }else{
                $badgeColor = "red";
            }

            ?>
      <tr class="{{$perguntadoHa>72?"danger":""}}">
       <a href=""></a>
       <td>{{$pergunta->id}}</td>
       <td>{{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}</td>
       <td class="col-md-2">
        <div class="progress progress-xs">
         <div
             class="progress-bar progress-bar-{{$badgeColor}}"
             style="width: {{(int)($perguntadoHa/72*100)>100?100:$perguntadoHa/72*100}}%">

         </div>
        </div>
       </td>

       <td><span class="badge bg-{{$badgeColor}}">{{ $perguntadoHa }}</td></span>
      </tr>
     @endforeach
    </table>
   </div>
   <!-- /.box-body -->
  </div>
  <!-- /.box -->
 </div>
 </div>

  <!-- PERGUNTAS RESPONDIDAS -->
  <div class="row margin" style="margin-top: -20px;">
   <div id="perguntasSemResposta">
    <div class="box">
     <div class="box-header">
      <h3 class="box-title">Minhas Perguntas Respondidas</h3>

      <div class="box-tools">
       <ul class="pull-right">
        <a href="#" class="">Ver Todas</a>
       </ul>
      </div>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
      <table class="table table-striped">
       <tr>
        <th style="width: 10px">#</th>
        <th>Pergunta</th>
        <th></th>
        <th style="width: 40px">Tempo Restante</th>
       </tr>
       @foreach($perguntasRespondidas as $pergunta)
        <tr>
         <a href=""></a>
         <td>{{$pergunta->id}}</td>
         <td>{{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}</td>
         <td class="col-md-2">
          <div class="progress progress-xs">
           <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
          </div>
         </td>
         <td><span class="badge bg-red">{{
                  12
             }}</td></span>
        </tr>
       @endforeach
      </table>
     </div>
     <!-- /.box-body -->
    </div>
    <!-- /.box -->
   </div>
  </div>
 </div>
@endsection