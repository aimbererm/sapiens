@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row margin">
        <div id="perguntasSemResposta">
            <div class="box">
            @if($session->grupo()->get(array("Descricao"))[0]->Descricao == "Cliente")
                <form action="{{url("/list")}}"></form>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 20px" ># Código</th>
                            <th class="col-md-6">Pergunta</th>
                            <th class="col-md-2">Tempo Restante</th>
                            <th class="col-md-3"style="width: 40px">Perguntado há</th>
                        </tr>
                        @if($session->grupo()->get(array("Descricao"))[0]->Descricao == "Advogado")
                            @foreach($perguntas as $pergunta)
                                <?php
                                date_default_timezone_set('America/Sao_Paulo');
                                $data = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pergunta->dtHoraPergunta);
                                $perguntadoHa = $data->diffInHours(\Carbon\Carbon::now());

                                if($perguntadoHa < 24){
                                    $badgeColor = "green";
                                }elseif($perguntadoHa < 48){
                                    $badgeColor = "yellow";
                                }else{
                                    $badgeColor = "red";
                                }

                                if(!is_null($pergunta->Resposta)){
                                    $url = "/pergunta/".$pergunta->id."/show";
                                    echo '<tr>';
                                }else{
                                    $url = "/pergunta/".$pergunta->id."/";
                                    echo "<tr class=\"$perguntadoHa>72?\"danger\":\"\"\">";
                                }

                                ?>

                                <td>
                                    <a href="{{url($url)}}">
                                        {{$pergunta->id}}
                                    </a>
                                </td>
                                    <td>
                                        <a href="{{url($url)}}">
                                            {{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}
                                        </a>
                                    </td>
                                @if(is_null($pergunta->Resposta))
                                    <td class="col-md-2">
                                        <div class="progress progress-xs">
                                            <div
                                                    class="progress-bar progress-bar-{{$badgeColor}}"
                                                    style="width: {{(int)($perguntadoHa/72*100)>100?100:$perguntadoHa/72*100}}%">

                                            </div>
                                        </div>
                                    </td>


                                    <td><span class="badge bg-aqua">Respondida em</span></td>
                                @else
                                    <td class="col-md-2">
                                        <span class="badge bg-aqua">Respondida em</span>
                                    </td>
                                    <td>
                                        <span class="badge bg-aqua">
                                            {{
                                                \Carbon\Carbon::
                                                createFromFormat('Y-m-d H:i:s',
                                                    $pergunta->dtHoraResposta)->format("d/m/Y")
                                            }} hrs
                                        </span>
                                    </td>
                                @endif

                                </tr>
                            @endforeach
                        @else
                            @foreach($perguntas as $pergunta)
                                <?php
                                date_default_timezone_set('America/Sao_Paulo');
                                $data = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $pergunta->dtHoraPergunta);
                                $perguntadoHa = $data->diffInHours(\Carbon\Carbon::now());

                                if($perguntadoHa < 24){
                                    $badgeColor = "green";
                                }elseif($perguntadoHa < 48){
                                    $badgeColor = "yellow";
                                }else{
                                    $badgeColor = "red";
                                }

                                ?>
                                <tr>
                                    <td>
                                        <a href="{{url("/pergunta/".$pergunta->id."/show")}}">
                                            {{$pergunta->id}}
                                        </a>
                                    </td>

                                    <td>
                                        <a href="{{url("/pergunta/".$pergunta->id."/show")}}">
                                            {{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}
                                        </a>
                                    </td>

                                    <td class="col-md-2">
                                        <div class="progress progress-xs">
                                            <div
                                                    class="progress-bar progress-bar-{{$badgeColor}}"
                                                    style="width: {{(int)($perguntadoHa/72*100)>100?100:$perguntadoHa/72*100}}%">

                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <span class="badge bg-{{$badgeColor}}">
                                            {{ $perguntadoHa>1?$perguntadoHa."hrs":$perguntadoHa."hr" }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection