@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row margin">
            <h2 class="page-header"> Faça sua pergunta</h2>
            <p> Escreva abaixo sua dúvida e nós responderemos em até 72h</p>
        </div>
        
        <div class="row margin">
            <form action="{{url("/perguntar")}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea name="Pergunta" id="pergunta" class="form-control" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="Publica"> Esta pergunta é pública?(Pode ser pesquisada por outros usuários)
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
@endsection