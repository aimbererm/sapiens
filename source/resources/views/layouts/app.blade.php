<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $page_title or "SindBasbor" }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/plugins/datatables/dataTables.bootstrap.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/dist/css/AdminLTE.min.css") }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/dist/css/skins/skin-blue.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css") }}">
    <link rel="stylesheet" href="{{ asset("/assets/AdminLTE-2.3.5/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css") }}">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Header -->
@include('layouts/header')

<!-- Sidebar -->
@include('layouts/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="row">
        @include('common.messages')
        <!-- Your Page Content Here -->
            @yield('content')
        </div>
        </div><!-- /.content-wrapper -->

        <!-- Footer -->
        @include('layouts/footer')
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset("/assets/AdminLTE-2.3.5/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset("/assets/AdminLTE-2.3.5/bootstrap/js/bootstrap.min.js") }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset("/assets/AdminLTE-2.3.5/dist/js/app.min.js") }}"></script>


</body>
</html>
