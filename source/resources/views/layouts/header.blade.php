<!-- Main Header -->
<?php
date_default_timezone_set('America/Sao_Paulo');
?>
<header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <img src="{{ asset("/bower_components/admin-lte/dist/img/logo00.png") }}" class="logo-mini" alt="">
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Statera</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Recolher</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">sdfgsdfg <i class="fa fa-sort-desc"></i></span>
                    </a>
                    <ul class="dropdown-menu">

                        <!-- The user image in the menu -->
                        <li class="user-footer">
                            <h4>sdg</h4>
                            <h5>asdfasd</h5>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ action("PerguntaController@index") }}" class="btn btn-default">
                                    <i class="fa fa-user"></i> Perfil</a>
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ action("PerguntaController@index") }}" class="btn btn-default">
                                    <i class="fa fa-power-off"></i> Sair</a>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>