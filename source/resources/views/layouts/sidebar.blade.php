<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu Principal</li>
            <!-- Optionally, you can add icons to the links -->
            @if (!is_null($session))
                @if($session->grupo()->get(array("Descricao"))[0]->Descricao == "Advogado")
                    <li>
                        <a href="{{url("/pergunta")}}">
                            <span>Início</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" onclick="$('#RespondidasPorMim').submit()">
                            <form action="/perguntas" method="get" id="RespondidasPorMim">
                                <input type="hidden" name="respondidasPorMim" value="true"/>
                                <span>Respondidas Por Mim</span>
                            </form>
                        </a>
                    </li>
                @elseif($session->grupo()->get(array("Descricao"))[0]->Descricao == "Cliente")
                    <li>
                        <a href="{{url("/pergunta")}}">
                            <span>Início</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url("/pergunta/create")}}">
                            <span>Fazer Nova Pergunta</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" onclick="$('#Nrespondidas').submit()">
                            <form action="/perguntas" method="get" id="Nrespondidas">
                                <input type="hidden" name="nrespondida" value="true"/>
                                <span>Perguntas Sem Resposta</span>
                            </form>
                        </a>
                    </li>

                    <li>
                        <a href="#" onclick="$('#Respondidas').submit()">
                            <form action="/perguntas" method="get" id="Respondidas">
                                <input type="hidden" name="respondidas" value="true"/>
                                <span>Perguntas Respondidas</span>
                            </form>
                        </a>
                    </li>

                    <li>
                        <a href="#" onclick="$('#Publica').submit()">
                            <form action="/perguntas" method="get" id="Publica">
                                <input type="hidden" name="publica" value="true"/>
                                <span>Perguntas publicas</span>
                            </form>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <span>Meus dados</span>
                        </a>
                    </li>

                @elseif($session->grupo()->get(array("Descricao"))[0]->Descricao == "Gerente")
                    <li>
                        <a href="#">
                            <span>Início</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <span>Perguntas Sem Resposta</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <span>Respondidas Por Mim</span>
                        </a>
                    </li>
                    
                @endif
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>