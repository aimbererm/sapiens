@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <section id="perguntas">
        <h2># Perguntas</h2>

        <div class="row margin">
            <div class="col-md-3">
                <div class="info-box bg-red-gradient">
                    <span class="info-box-icon"><i class="fa fa-commenting"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-yellow-gradient">
                    <span class="info-box-icon"><i class="fa  fa-hourglass-end"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-blue-gradient">
                    <span class="info-box-icon"><i class="fa  fa-star-half-empty"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

        </div>
    </section>

    <section id="perguntas">
        <h2># Consultor</h2>
        <div class="row margin">
            <div class="col-md-3">
                <div class="info-box bg-blue-gradient">
                    <span class="info-box-icon"><i class="fa  fa-star-half-empty"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-green-gradient">
                    <span class="info-box-icon"><i class="fa  fa-calendar"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-blue-gradient">
                    <span class="info-box-icon"><i class="fa  fa-star-half-empty"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>
    </section>

    <section id="perguntas">
        <h2># Cliente</h2>

        <div class="row margin">
            <div class="col-md-3">
                <div class="info-box bg-red-gradient">
                    <span class="info-box-icon"><i class="fa fa-commenting"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-yellow-gradient">
                    <span class="info-box-icon"><i class="fa  fa-hourglass-end"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="info-box bg-blue-gradient">
                    <span class="info-box-icon"><i class="fa  fa-star-half-empty"></i></span>
                    <div class="info-box-content">
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
    </section>

</div>
@endsection