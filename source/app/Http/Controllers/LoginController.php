<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use app\Services\UsuarioService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Symfony\Component\Console\Helper\Helper;

class LoginController extends Controller
{
    private $user;

    public function __construct(UsuarioService $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("auth.login");
    }

    public function login(Request $request)
    {
        $user = $this->user->login($request->get("usuario"),$request->get("password"));
        $request->session()->put("user",$user);

        if (!is_null($user))
                return redirect("/pergunta");
        return redirect("/login");
    }

    public function logout(Request $request, $id)
    {
        $request->session()->forget("user");
        $request->session()->flush();

    }
}
