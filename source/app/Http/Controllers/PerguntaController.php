<?php

namespace App\Http\Controllers;

use App\Models\Pergunta;
use App\Repositories\PessoaRepository;
use App\Services\PerguntaService;
use App\Services\PessoaService;
use Illuminate\Http\Request;

use App\Http\Requests;

class PerguntaController extends Controller
{

    private $perguntaService;
    private $pessoaService;

    public function __construct(PerguntaService $perguntaService,
                                PessoaService $pessoaService)
    {
        $this->pessoaService = $pessoaService;
        $this->perguntaService = $perguntaService;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->session()->has("user")) {
            return view("auth.login");
        }
        $user = $request->session()->get("user");


        if($user->grupo()->get(array("Descricao"))[0]->Descricao == "Cliente") {
            $perguntasSemReposta = $this->perguntaService
                                        ->perguntasPessoaSemRespostaDashBoard($user->pessoa()->get(array("id"))[0]->id);

            $perguntasRespondidas = $this->perguntaService
                                         ->perguntasPessoaRespondidasDashboard($user->pessoa()->get(array("id"))[0]->id);

            return view("pergunta.index", array(
                "perguntasSemResposta" => $perguntasSemReposta,
                "perguntasRespondidas" => $perguntasRespondidas,
                "session" => $request->session()->get("user")
            ));
        }elseif ($user->grupo()->get(array("Descricao"))[0]->Descricao == "Advogado"){
            $perguntasSemReposta = $this->perguntaService->perguntasSemResposta();

            $qtdPerguntasSemResposta = $this->perguntaService->qtdPerguntasNaoRespondidas();

            $qtdPerguntasRespondidasConsultor = $this->perguntaService
                                                ->qtdPerguntasRespondidasConsultor($user->pessoa()->get(array("id"))[0]->id);

            $qtdPerguntasRespondidas = $this->perguntaService->qtdPerguntasRespondidas();

            $qtdTotalPerguntas = $this->perguntaService->qtdTotalPerguntas();

            return view("pergunta.consultor", array(
                "perguntas" => $perguntasSemReposta,
                "session" => $request->session()->get("user"),
                "qtdPerguntasSemResposta" => $qtdPerguntasSemResposta,
                "qtdPerguntasRespondidasConsultor" => $qtdPerguntasRespondidasConsultor,
                "qtdPerguntasRespondidas" => $qtdPerguntasRespondidas,
                "qtdTotalPerguntas" => $qtdTotalPerguntas,
            ));
        }elseif ($user->grupo()->get(array("Descricao"))[0]->Descricao == "Gerente"){
            return view("gerente.index",array(
                    "session" => $request->session()->get("user")
                )
                );
        }
    }

    public function showList(Request $request){
        if(!$request->session()->has("user")) {
            return view("auth.login");
        }
        $user = $request->session()->get("user");


        if($user->grupo()->get(array("Descricao"))[0]->Descricao == "Cliente"){

            if($request->get("publica")) {
                $perguntas = $this->perguntaService->perguntasPublicas();
            }elseif($request->get("respondidas")) {
                $perguntas = $this->perguntaService->perguntasPessoaRespondidas($user->pessoa()->get(array("id"))[0]->id);
            }elseif($request->get("nrespondida")) {
                $perguntas = $this->perguntaService->perguntaPessoaSemResposta($user->pessoa()->get(array("id"))[0]->id);
            }
            return view("pergunta.list",array(
                "session" => $user,
                "perguntas"=>$perguntas
            ));
        }
        elseif($user->grupo()->get(array("Descricao"))[0]->Descricao == "Advogado"){

            if($request->get("respondidasPorMim"))
                $perguntas = $this->perguntaService->perguntasConsultor($user->pessoa()->get(array("id"))[0]->id);
            return view("pergunta.list",array(
                "session" => $user,
                "perguntas" => $perguntas
            ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!$request->session()->has("user")) {
            return view("auth.login");
        }
        $user = $request->session()->get("user");

        return view("pergunta.create",array("session" => $request->session()->get("user")));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pergunta = $request->all();

        if($request->get("Pergunta"))
            $pergunta["Pessoa_pergunta"] = $request->session()->get("user")->Pessoa_idPessoa;
        else
            $pergunta["Pessoas_resposta"] = $request->session()->get("user")->Pessoa_idPessoa;

        //salva no banco
        $this->perguntaService->save($pergunta);

        return redirect("/pergunta");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if(!$request->session()->has("user")) {
            return view("auth.login");
        }
        $user = $request->session()->get("user");

        $pergunta = $this->perguntaService->find($id);
        return view("pergunta.show",array(
            "pergunta"=>$pergunta,
            "session" =>$user,
            "voltar" => redirect()->back(),
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $pergunta = $this->perguntaService->find($id);
        return view("pergunta.edit",array(
            "pergunta"=>$pergunta,
            "session" =>$request->session()->get("user")
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
