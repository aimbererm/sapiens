<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model {

    /**
     * Generated
     */
    public $timestamps = false;
    
    protected $table = 'telefones';
    protected $fillable = ['Pessoa_idPessoa', 'DDD', 'Telefone'];


    public function pessoa() {
        return $this->belongsTo('App\Models\Pessoa', 'Pessoa_idPessoa', 'id');
    }

    public function empresasresponsaveis() {
        return $this->hasMany('App\Models\EmpresasResponsaveis', 'Telefones_idTelefones', 'id');
    }


}
