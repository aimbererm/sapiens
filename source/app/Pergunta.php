<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pergunta extends Model {

    /**
     * Generated
     */
    public $timestamps = false;
    
    protected $table = 'perguntas';
    protected $fillable = ['Pergunta', 'Resposta', 'Publica', 'Pessoa_pergunta', 'Pessoas_resposta', 'dtHoraPergunta', 'dtHoraResposta'];


    public function pessoaPergunta() {
        return $this->belongsTo('App\Models\Pessoa', 'Pessoa_pergunta', 'id');
    }

    public function pessoaResposta() {
        return $this->belongsTo('App\Models\Pessoa', 'Pessoas_resposta', 'id');
    }


}
