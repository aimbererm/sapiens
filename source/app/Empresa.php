<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {

    /**
     * Generated
     */
    public $timestamps = false;
    
    protected $table = 'empresas';
    protected $fillable = ['CNPJ', 'RazaoSocial'];


    public function pessoas() {
        return $this->hasMany('App\Models\Pessoa', 'Pessoa_idPessoa', 'id');
    }


}
