<?php namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable{

    //tipos de perfis dos usuários
    const GERENTE  = 1;
    const CLIENTE  = 2;
    const ADVOGADO = 3;

    public $timestamps = false;

    protected $table = 'usuarios';
    protected $fillable = ['login', 'Pessoa_idPessoa', 'Grupo_idGrupo'];
    
    protected $hidden = ["password", "remember_token"];


    public function grupo() {
        return $this->belongsTo('App\Models\Grupo', 'Grupo_idGrupo', 'id');
    }

    public function pessoa() {
        return $this->belongsTo('App\Models\Pessoa', 'Pessoa_idPessoa', 'id');
    }
}
