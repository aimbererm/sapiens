<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 07/07/2016
 * Time: 17:06
 */

namespace app\Services;


use App\Repositories\UsuarioRepository;

class UsuarioService
{
    public function __construct(UsuarioRepository $user)
    {
        $this->user = $user;
    }

    public function login($login,$senha){
        return $this->user->login($login,$senha);
    }

}