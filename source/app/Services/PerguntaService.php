<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 07/07/2016
 * Time: 10:45
 */

namespace App\Services;


use App\Models\Pergunta;
use App\Repositories\PerguntaRepository;
use Carbon\Carbon;
use Faker\Provider\DateTime;

class PerguntaService
{
    private $perguntaRepo;

    public function __construct(PerguntaRepository $perguntaRepository)
    {
        $this->perguntaRepo = $perguntaRepository;
    }

    public function all(){
        return $this->perguntaRepo->all();
    }

    public function find($id){
        return $this->perguntaRepo->find($id);
    }
    
    public function perguntasPessoaDashboard($id){
        return $this->perguntaRepo->perguntasPessoaDashboard($id);
    }

    public function  perguntasPessoaSemRespostaDashBoard($id){
        return $this->perguntaRepo->perguntasPessoaSemRespostaDashboard($id);
    }

    public function perguntasPessoaRespondidasDashboard($id){
        return $this->perguntaRepo->perguntasPessoaRespondidasDashboard($id);
    }
    
    public function perguntasCliente($id){
        return $this->perguntaRepo->perguntasCliente($id);
    }

    public function perguntasPublicas(){
        return $this->perguntaRepo->perguntasPublicas();
    }

    public function perguntaPessoaSemResposta($id){
        return $this->perguntaRepo->perguntasPessoaSemResposta($id);
    }

    public function perguntasPessoaRespondidas($id){
        return $this->perguntaRepo->perguntasPessoaRespondidas($id);
    }

    public function perguntasSemResposta(){
        return $this->perguntaRepo->perguntasSemResposta();
    }

    public function qtdPerguntasNaoRespondidas(){
        return $this->perguntaRepo->qtdPerguntasNaoRespondidas();
    }

    public function qtdTotalPerguntas(){
        return $this->perguntaRepo->qtdTotalPerguntas();
    }

    public function qtdPerguntasRespondidasConsultor($id){
        return $this->perguntaRepo->qtdPerguntasRespondidasConsultor($id);
    }

    public function qtdPerguntasRespondidas(){
        return $this->perguntaRepo->qtdPerguntasRespondidas();
    }

    public function perguntasConsultor($id){
        return $this->perguntaRepo->perguntasConsultor($id);
    }

    public function save($perguntaArray){
        date_default_timezone_set('America/Sao_Paulo');

        if(array_key_exists("id",$perguntaArray)) {
            $pergunta = Pergunta::find($perguntaArray["id"]);
            $pergunta->fill($perguntaArray);
            $pergunta->dtHoraResposta = Carbon::now();
        }else {
            $pergunta = new Pergunta();
            $pergunta->fill($perguntaArray);
            $pergunta->dtHoraPergunta = Carbon::now();
            $pergunta->Publica = array_key_exists("Publica",$perguntaArray)?1:0;
        }

        return $this->perguntaRepo->save($pergunta);
    }
}