<?php
/**
 * Created by PhpStorm.
 * User: celeste
 * Date: 08/07/2016
 * Time: 02:50
 */

namespace App\Services;


use App\Repositories\PessoaRepository;

class PessoaService
{
    private $pessoaRepo;

    public function __construct(PessoaRepository $pessoaRepository){
        $this->pessoaRepo = $pessoaRepository;
    }

    public function all(){
        return $this->pessoaRepo->all();
    }

    public function find($id){
        return $this->pessoaRepo->find($id);
    }

    public function save($pessoa){
        try{
            return $this->pessoaRepo->save($pessoa);
        }catch (\Exception $e){
            return $e;
        }
    }

}