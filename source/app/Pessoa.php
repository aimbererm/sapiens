<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model {

    /**
     * Generated
     */
    public $timestamps = false;
    
    protected $table = 'pessoas';
    protected $fillable = ['Nome', 'CPF', 'email'];

    public function grupos() {
        return $this->belongsToMany('App\Models\Grupo', 'usuarios', 'Pessoa_idPessoa', 'id');
    }

    public function perguntas() {
        return $this->hasMany('App\Models\Pergunta', 'Pessoa_pergunta', 'id');
    }

    public function respostas() {
        return $this->hasMany('App\Models\Pergunta', 'Pessoas_resposta', 'id');
    }

    public function telefones() {
        return $this->hasMany('App\Models\Telefone', 'Pessoa_idPessoa', 'id');
    }

    public function usuarios() {
        return $this->hasMany('App\Models\Usuario', 'Pessoa_idPessoa', 'id');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Empresa','Empresas_id','id');
    }


}
