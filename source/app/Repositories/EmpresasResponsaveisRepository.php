<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:37
 */

namespace App\Repositories;


use App\Models\EmpresasResponsaveis;

class EmpresasResponsaveisRepository
{
    private $empresa_responsavel;

    public function __construct(EmpresasResponsaveis $empresa_responsavel)
    {
        $this->empresa_responsavel = $empresa_responsavel;
    }
    
    public function save($empresa_responsavel){
        try{
            return $this->empresa_responsavel->save($empresa_responsavel);
        }catch (\Exception $e){
            return $e;
        }
    }
}