<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:36
 */

namespace App\Repositories;


use App\Models\Pessoa;

class PessoaRepository
{
    private $pessoa;

    public function __construct(Pessoa $pessoa)
    {
        $this->pessoa = $pessoa;
    }

    public function all(){
        return $this->pessoa->all();
    }

    public function find($id){
        return $this->pessoa->find($id);
    }

    public function save($pessoa){
        try{
            return $this->pessoa->save($pessoa);
        }catch (\Exception $e){
            return $e;
        }
    }
}