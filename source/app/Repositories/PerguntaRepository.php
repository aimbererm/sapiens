<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:36
 */

namespace App\Repositories;

use App\Models\Empresa;
use App\Models\EmpresasResponsaveis;
use App\Models\Pergunta;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class PerguntaRepository
{
    private $pergunta;
    
    public function __construct(Pergunta $pergunta)
    {
        $this->pergunta = $pergunta;
    }
    
    public function all(){
        return $this->pergunta->all();
    }

    public function qtdTotalPerguntas(){
        return $this->pergunta->all()->count();
    }

    public function qtdPerguntasNaoRespondidas(){
        return $this->pergunta->whereNull("Resposta")->get()->count();
    }

    public function qtdPerguntasRespondidas(){
        return $this->pergunta->whereNotNull("Resposta")->get()->count();
    }

    public function qtdPerguntasRespondidasConsultor($id){
        return $this->pergunta->where("Pessoas_resposta",$id)->get()->count();
    }
    
    public function find($id){
        return $this->pergunta->find($id);
    }
    
    public function search($string){
        return $this->pergunta->where(["pergunta"=> $string])->get();
    }

    public function searchPublica($string){
        return $this->pergunta->where(["Pergunta","like","%".$string."%"]);
    }

    public function searchRespondidas($string,$id){
        return $this->pergunta
                    ->where("Pessoa_pergunta",$id)
                    ->where(["Pergunta","like","%".$string."%"]);
    }

    public function perguntasPublicas(){
        return $this->pergunta->where("Publica",1)->whereNotNull("Resposta")->get();
    }

    public function perguntasPessoaRespondidasDashboard($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)
            ->whereNotNull("Resposta")
            ->take(3)->get();
    }

    public function perguntasPessoaRespondidas($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)
            ->whereNotNull("Resposta")
            ->get();
    }

    public function perguntasPessoa($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)->get();
    }

    public function perguntasPessoaDashboard($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)->take(3)->get();
    }

    public function perguntasPessoaSemRespostaDashboard($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)
            ->whereNull("Resposta")->take(3)->get();
    }

    public function perguntasPessoaSemResposta($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)
            ->whereNull("Resposta")->get();
    }

    public function perguntasCliente($id){
        //Obtenho o Id da empresa
        $empresa  = EmpresasResponsaveis::select("Empresas_id")
                            ->where("Pessoa_idPessoa",$id)->get("Empresas_id")->first();
        //Pego todos os funcionários que trabalham na empresa
        $clientes = EmpresasResponsaveis::where("Empresas_id",$empresa)->get("Pessoa_idPessoa");

        //retorno um array de perguntas
        return $this->pergunta->whereIn("Pessoa_idPessoa",$clientes)->get();
    }

    public function perguntasConsultor($id){
        return $this->pergunta->where("Pessoas_resposta",$id)->get();
    }

    public function perguntasSemResposta(){
        return $this->pergunta->whereNull("Resposta")->get();
    }

    public function save($pergunta){
        try{
            $pergunta->save();

        }catch (\Exception $e){
            return $e;
        }
    }
}