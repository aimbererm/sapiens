<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:37
 */

namespace App\Repositories;


use App\Models\Usuario;

class UsuarioRepository
{
    private $user;

    public function __construct(Usuario $user)
    {
        $this->user = $user;
    }

    public function all(){
        return $this->user->all();
    }

    public function find($id){
        return $this->user->find($id);
    }

    public function save($user){
        try{
            return $this->user->save($user);
        }catch (\Exception $e){
            return $e;
        }
    }

    public function login($login,$senha){
        return $this->user->where("login",$login)
            ->where("password",$senha)->first();
    }

}