<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 15:56
 */

namespace App\Services;


use App\Repositories\EmpresaRepository;

class EmpresaService
{
    private $empresaRepo;

    public function __construct(EmpresaRepository $empresaRepo)
    {
        $this->empresaRepo = $empresaRepo;
    }

    public function all(){
        return $this->empresaRepo->all();
    }

    public function find($id){
        return $this->empresaRepo->find($id);
    }

    public function save($empresa){
        return $this->empresaRepo->save($empresa);
    }

}