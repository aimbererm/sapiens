<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 07/07/2016
 * Time: 12:37
 */

namespace App\Services;

use App\Models\EmpresasResponsaveis;
use App\Repositories\EmpresaRepository;
use App\Repositories\EmpresasResponsaveisRepository;
use App\Repositories\PessoaRepository;
use App\Repositories\UsuarioRepository;
use Illuminate\Support\Facades\DB;


class CadastroService
{
    public function cadastrar($empresa,$pessoa,$usuario){


        try{
            
           return DB::transaction(function ($empresa,$pessoa,$usuario){
                $userRepo = new UsuarioRepository();
                $pessoaRepo = new PessoaRepository();
                $empresaRepo = new EmpresaRepository();
                $pessoaEmpresaRepo = new EmpresasResponsaveisRepository();
                
                
                $pessoaEmpresa = new EmpresasResponsaveis();
                
                $empresa = $empresaRepo->save($empresa);
                $pessoa = $pessoaRepo->save($pessoa);
                $usuario = $userRepo->save($usuario);


                $pessoaEmpresa->Empresas_idEmpresa = $empresa->id;
                $pessoaEmpresa->Pessoa_idPessoa = $pessoa->id;
                $pessoaEmpresa->responsavel = true;

                $pessoaEmpresaRepo->save($pessoaEmpresa);
            });
        }catch (\Exception $e){
            return $e;
        }
    }

}