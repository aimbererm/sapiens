<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 07/07/2016
 * Time: 10:45
 */

namespace App\Services;


use App\Repositories\PerguntaRepository;

class PerguntaService
{
    private $perguntaRepo;

    public function __construct(PerguntaRepository $perguntaRepository)
    {
        $this->perguntaRepo = $perguntaRepository;
    }

    public function all(){
        return $this->perguntaRepo->all();
    }

    public function find($id){
        return $this->perguntaRepo->find($id);
    }

}