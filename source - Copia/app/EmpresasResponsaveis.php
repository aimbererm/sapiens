<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresasResponsaveis extends Model {

    /**
     * Generated
     */

    protected $table = 'empresasresponsaveis';
    protected $fillable = ['Empresas_idEmpresa', 'Pessoa_idPessoa','responsavel', 'Telefones_idTelefones'];


    public function empresa() {
        return $this->belongsTo('App\Models\Empresa', 'Empresas_idEmpresa', 'idEmpresa');
    }

    public function pessoa() {
        return $this->belongsTo('App\Models\Pessoa', 'Pessoa_idPessoa', 'idPessoa');
    }

    public function telefone() {
        return $this->belongsTo('App\Models\Telefone', 'Telefones_idTelefones', 'idTelefones');
    }


}
