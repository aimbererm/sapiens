<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:34
 */

namespace App\Repositories;


use App\Models\Grupo;

class GrupoRepository
{
    private $grupo;

    public function __construct(Grupo $grupo)
    {
        $this->grupo = $grupo;
    }

    public function all(){
        return $this->grupo->all();
    }

    public function find($id){
        return $this->grupo->find($id);
    }

    public function save($grupo){
        try{
            return $this->grupo->save($grupo);
        }catch (\Excepetion $e){
            return $e;
        }
    }

}