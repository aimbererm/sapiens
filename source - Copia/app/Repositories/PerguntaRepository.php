<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:36
 */

namespace App\Repositories;

use App\Models\Empresa;
use App\Models\EmpresasResponsaveis;
use App\Models\Pergunta;
use Mockery\CountValidator\Exception;

class PerguntaRepository
{
    private $pergunta;
    
    public function __construct(Pergunta $pergunta)
    {
        $this->pergunta = $pergunta;
    }
    
    public function all(){
        return $this->pergunta->all();
    }
    
    public function find($id){
        return $this->pergunta->find($id);
    }
    
    public function search($string){
        return $this->pergunta->where(["pergunta"=> $string])->get();
    }

    public function perguntasPublicas(){
        return $this->pergunta->where("publica",1)->get();
    }

    public function perguntasPessoa($id){
        return $this->pergunta->where("Pessoa_pergunta",$id)->get();
    }

    public function perguntasCliente($id){
        //Obtenho o Id da empresa
        $empresa  = EmpresasResponsaveis::where("Pessoa_idPessoa",$id)->pluck("Empresas_id")->first();
        //Pego todos os funcionários que trabalham na empresa
        $clientes = EmpresasResponsaveis::where("Empresas_id",$empresa)->pluck("Pessoa_idPessoa")->get();

        //retorno um array de perguntas
        return $this->pergunta->whereIn("Pessoa_idPessoa",$clientes)->get();
    }

    public function perguntasProfessores($idProfessores){
        return $this->pergunta->where("Pessoas_resposta",$idProfessores)->get();
    }

    public function qtdTotalPerguntas(){
        return $this->pergunta->all()->count();
    }

    public function perguntasSemResposta(){
        return $this->pergunta->whereNull("Resposta")->get();
    }

    public function save($pergunta){
        try{
            return $this->pergunta->save($pergunta);
        }catch (\Exception $e){
            return $e;
        }
    }
}