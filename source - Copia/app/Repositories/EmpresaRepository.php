<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:34
 */

namespace App\Repositories;


use App\Models\Empresa;

class EmpresaRepository
{
    private $empresa;

    public function __construct(Empresa $empresa)
    {
        $this->empresa = empresa;
    }

    public function all(){
        return $this->empresa->all();
    }

    public function find($id){
        return $this->empresa->find($id);
    }

    public function save(Empresa $empresa){
        try{
            return $empresa->save();
        }catch (\Exception $e) {
            return $e;
        }
    }

    public function delete(Empresa $empresa){
        try{
            return $empresa->delete();
        }catch (\Exception $e){
            
        }
    }

}