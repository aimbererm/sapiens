<?php
/**
 * Created by PhpStorm.
 * User: HP-6
 * Date: 04/07/2016
 * Time: 13:36
 */

namespace App\Repositories;


use App\Models\Telefone;

class TelefoneRepository
{
    private $telefone;

    public function __construct(Telefone $telefone)
    {
        $this->telefone = $telefone;
    }

    public function all(){
        return $this->telefone->all();
    }

    public function find($id){
        return $this->telefone->find($id);
    }

    public function save($telefone){
        try{
            return $this->telefone->save($telefone);
        }catch (\Exception $e){
            return $e;
        }
    }

}