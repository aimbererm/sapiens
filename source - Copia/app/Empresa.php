<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {

    /**
     * Generated
     */

    protected $table = 'empresas';
    protected $fillable = ['CNPJ', 'RazaoSocial'];


    public function empresasresponsaveis() {
        return $this->hasMany('App\Models\EmpresasResponsaveis', 'Empresas_idEmpresa', 'idEmpresa');
    }


}
