<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'CadastroController@index');
Route::post('/registrar', 'CadastroController@store');

Route::get("/login", 'LoginController@index');
Route::post("/login",'LoginController@login');

Route::get("/pergunta", "PerguntaController@index");
Route::get("/pergunta/create", "PerguntaController@create");
Route::post("/perguntar", "PerguntaController@store");
Route::get("/pergunta/{id}", "PerguntaController@edit");

Route::auth();

Route::get('/home', 'HomeController@index');
