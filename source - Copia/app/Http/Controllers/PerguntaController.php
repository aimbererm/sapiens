<?php

namespace App\Http\Controllers;

use App\Models\Pergunta;
use App\Repositories\PessoaRepository;
use App\Services\PerguntaService;
use App\Services\PessoaService;
use Illuminate\Http\Request;

use App\Http\Requests;

class PerguntaController extends Controller
{

    private $perguntaService;
    private $pessoaService;

    public function __construct(PerguntaService $perguntaService,
                                PessoaService $pessoaService)
    {
        $this->pessoaService = $pessoaService;
        $this->perguntaService = $perguntaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->session()->has("user")) {
            return view("auth.login");
        }
        $perguntas = $this->perguntaService->all();
        return view("pergunta.index",array(
            "minhasPerguntas" => $perguntas,
            "session" => $request->session()->get("user")
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view("pergunta.create",array("session" => $request->session()->get("user")));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pergunta = $request->all();
        $pergunta["Pessoa_pergunta"] = $request->session()->get("user")->Pessoa_idPessoa;

        //salva no banco
        $this->perguntaService->save($pergunta);

        return redirect("/pergunta");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $pergunta = $this->perguntaService->find($id);
        return view("pergunta.edit",array(
            "pergunta"=>$pergunta,
            "session" =>$request->session()->get("user")
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
