<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grupo extends Model {

    /**
     * Generated
     */

    protected $table = 'grupo';
    protected $fillable = ['Descricao'];


    public function pessoas() {
        return $this->belongsToMany('App\Models\Pessoa', 'usuarios', 'Grupo_idGrupo', 'Pessoa_idPessoa');
    }

    public function usuarios() {
        return $this->hasMany('App\Models\Usuario', 'Grupo_idGrupo', 'idGrupo');
    }

}
