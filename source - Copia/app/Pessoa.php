<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model {

    /**
     * Generated
     */

    protected $table = 'pessoas';
    protected $fillable = ['Nome', 'CPF', 'email'];

    public function grupos() {
        return $this->belongsToMany('App\Models\Grupo', 'usuarios', 'Pessoa_idPessoa', 'Grupo_idGrupo');
    }

    public function empresasresponsaveis() {
        return $this->hasMany('App\Models\EmpresasResponsaveis', 'Pessoa_idPessoa', 'idPessoa');
    }

    public function perguntas() {
        return $this->hasMany('App\Models\Pergunta', 'Pessoa_pergunta', 'idPessoa');
    }

    public function respostas() {
        return $this->hasMany('App\Models\Pergunta', 'Pessoas_resposta', 'idPessoa');
    }

    public function telefones() {
        return $this->hasMany('App\Models\Telefone', 'Pessoa_idPessoa', 'idPessoa');
    }

    public function usuarios() {
        return $this->hasMany('App\Models\Usuario', 'Pessoa_idPessoa', 'idPessoa');
    }


}
