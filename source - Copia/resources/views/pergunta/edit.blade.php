@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row margin">

            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-question"></i></span>

                <div class="info-box-content">
                    <p>{{$pergunta->Pergunta}}</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

        <div class="row margin">
            <form action="{{url("/responder")}}" method="POST">
                <input type="hidden" name="id" value="{{$pergunta->id}}">
                <div class="form-group">
                    <textarea name="Pergunta" id="pergunta" class="form-control" rows="10"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
@endsection