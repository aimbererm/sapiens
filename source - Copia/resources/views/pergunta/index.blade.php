@extends('layouts.app')

@section('content')
 <div class="container-fluid">
 <div class="row margin">
  <div class="col-md-2">
   <a href="{{url("/pergunta/create")}}"
    <button type="button" class="btn btn-block btn-primary btn-lg">NOVA PERGUNTA</button>
   </a>
  </div>
 </div>
 <div class="row margin">
  <div id="Perguntas">
  <div class="box">
   <div class="box-header">
    <h3 class="box-title">Minhas Perguntas</h3>

    <div class="box-tools">
     <ul class="pagination pagination-sm no-margin pull-right">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">&raquo;</a></li>
     </ul>
    </div>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
    <table class="table table-striped">
     <tr>
      <th style="width: 10px">#</th>
      <th>Pergunta</th>
      <th></th>
      <th style="width: 40px">Tempo</th>
     </tr>
     @foreach($perguntas as $pergunta)
      <tr>
       <a href=""></a>
       <td>{{$pergunta->id}}</td>
       <td>{{strlen($pergunta->Pergunta)<80 ?$pergunta->Pergunta : substr($pergunta->Pergunta,0,79) . "..."}}</td>
       <td class="col-md-2">
        <div class="progress progress-xs">
         <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
        </div>
       </td>
       <td><span class="badge bg-red">12h</span></td>
      </tr>
     @endforeach
    </table>
   </div>
   <!-- /.box-body -->
  </div>
  <!-- /.box -->
 </div>
 </div>
 </div>
@endsection